# Utilisation d'une image de base Python
FROM python:3.8-slim

# Définition du répertoire de travail
WORKDIR /app

# Copie des fichiers de l'application dans le conteneur
COPY . /app

# Installation des dépendances
RUN pip install --no-cache-dir flask

EXPOSE 5000

# Commande pour exécuter l'application
CMD ["python", "app.py"]
