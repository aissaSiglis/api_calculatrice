# Importez les dépendances nécessaires en haut de votre fichier
import pytest
from app import app  # Assurez-vous que 'myapp' correspond au nom de votre fichier Flask

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

# Test de la route de multiplication
def test_multiply(client):
    response = client.get('/api/multiply?a=4&b=5')
    assert response.status_code == 200
    assert b'"result":20' in response.data

# Test de la route de division avec une entrée valide
def test_divide(client):
    response = client.get('/api/divide?a=20&b=4')
    assert response.status_code == 200
    assert b'"result":5.0' in response.data

# Test de la route de division avec une division par zéro
def test_divide_by_zero(client):
    response = client.get('/api/divide?a=20&b=0')
    assert response.status_code == 400
    assert b'"error":' in response.data